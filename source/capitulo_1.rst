Capitulo 1. Primeros pasos con Alta Disponibilidad
==================================================

En nuestra vida diaria hay multitud de servicios en los que confiamos, es decir, tenemos la certeza de que funcionarán con normalidad para llevarnos al trabajo, subir al piso 12 en ascensor, etc. De igual forma, ocurre con la tecnología. Esperamos que se pueda consultar el correo electrónico con normalidad, o visitar nuestra web favorita.

Y, ¿cómo es posible que esto ocurra?. Sabemos que no podemos confiar ciegamente en la tecnología, tarde o temprano, fallará. Con sistemas cada vez más complejos e interconectados, ¿cómo es posible mantenerlos siempre accesibles?.

La respuesta a esta pregunta es: `Alta Disponibilidad`. Los sistemas de alta disponibilidad nos llevan a considerar que el fallo no es una opción en nuestros sistemas. Lo que se verá más adelante, se centrará en CentOS7, y en sus diferentes herramientas para conseguir que nuestros servicios estén siempre disponibles.

Alta Disponibilidad
-------------------

El significado general de la palabra `disponibilidad` es una característica de un recurso al poder ser accedido o usado. La disponibilidad de un recurso se puede medir, y viene a ser un ratio entre el tiempo que el recurso está disponible con el tiempo que dicho recurso no está disponible. Si añadimos la palabra `alta`, se viene a indicar que el recurso está disponible la mayor parte del tiempo posible.

No todos los sistemas pueden ser identificados como altamente disponibles. Suele ser habitual realizar mediciones para calcular la disponibilidad de un sistema. Herramientas de monitorización, como Nagios o Zabbix, pueden proporcionar informes sobre la disponibilidad de nuestros servicios, e incluso enviar alertas cuando se produzcan caídas.

Diseño del sistema
------------------

Aquellos sistemas que pretendan ofrecer alta disponibilidad de sus servicios deben seguir un diseño de sistema específico mediante el cual poder proporcionar continuidad en las operaciones. La regla fundamental en un sistema de alta disponibilidad es **evitar los puntos únicos de fallo**. Un punto único de fallo es un componente del sistema que puede provocar un fallo general si llega a caerse. Nuestro diseño debe evitar la existencia de dichos puntos, haciendo más robusto el sistema completo.

Un sistema IT complejo puede llegar a tener varios puntos únicos de fallo. ¿Cómo los podemos evitar? Con redundancia. Es decir, duplicar aquellos componentes críticos, permitiendo que el sistema siga operativo después de un fallo en uno de los dispositivos que han sido duplicados. Hay dos tipos de redundancia: **pasiva** y **activa**. La redundancia pasiva significa que dos o más dispositivos se encargan de ofrecer un servicio, pero sólo uno de ellos está activo en un momento determinado. El resto de dispositivos están a la espera de entrar en servicio si se produce un fallo en el dispositivo que está ofreciendo el servicio. La redundancia activa, implica el uso de dos o más dispositivos, todos ellos ofreciendo el servicio en todo momento. Incluso si uno de los dispositivos falla, el resto siguen ofreciendo el servicio.

Actualmente, la mayoría de sistemas modernos son capaces de eliminar los puntos únicos de fallo de tipo hardware mediante la duplicación de dichos componentes. Pero, esta solución no es suficiente para evitar los puntos de fallo de tipo software, que es la principal razón para el uso de un cluster. Un cluster es un grupo de equipos ejecutando un software determinado que establece una comunicación bidireccional continua entre los miembros del cluster, que se conoce como **heartbeat**. Con esta herramienta es posible conocer el estado actual de cada uno de los miembros del cluster en un momento dado.

Clusters de ordenadores
-----------------------

Un cluster de ordenadores es un grupo de equipos que trabajan juntos para proporcionar alta disponibilidad en sus servicios. Los servicios se implementan con un número determinado de aplicaciones que trabajan en la capa de aplicación. La capa de aplicación es una de las muchas capas en las que se puede producir un fallo. Esta es la razón por la que implementar un cluster dentro de un esquema de alta disponibilidad.

Un cluster está formado por dos o más ordenadores, que están conectados entre ellos a través de una red local. El número máximo de miembros en un cluster viene determinado por la solución implementada, pero por norma general, un cluster soporta por lo menos 16 miembros. Es una buena práctica, que todos los miembros del cluster tengan las mismas características hardware, con componentes del mismo fabricante y mismas especificaciones.


