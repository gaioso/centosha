Licencia
========

==========================================================
Reconocimiento-CompartirIgual 3.0 España (CC BY-SA 3.0 ES)
==========================================================

Esto es un resumen inteligible para humanos (y no un sustituto) de la licencia, disponible en los idiomas siguientes: `Aranés <https://creativecommons.org/licenses/by-sa/3.0/es/legalcode.oci>`_ `Asturiano <https://creativecommons.org/licenses/by-sa/3.0/es/legalcode.ast>`_ `Castellano <https://creativecommons.org/licenses/by-sa/3.0/es/legalcode.es>`_ `Euskera <https://creativecommons.org/licenses/by-sa/3.0/es/legalcode.eu>`_ `Galego <https://creativecommons.org/licenses/by-sa/3.0/es/legalcode.gl>`_

Usted es libre de:
------------------

**Compartir** - copiar y redistribuir el material en cualquier medio o formato

**Adaptar** - remezclar, transformar y crear a partir del material para cualquier finalidad, incluso comercial

*El licenciador no puede revocar estar libertades mientras cumpla los términos de la licencia.*

----

Bajo las condiciones siguientes:
--------------------------------

.. image:: ./images/by.large.png
   :height: 50px
   :width: 50px
   :align: left

**Reconocimiento** - Debe reconocer adecuadamente la autoría [1]_, proporcionar un enlace a la licencia e indicar si se han realizado cambios [2]_. Puede hacerlo de cualquier manera razonable, pero no de una manera que sugiera que tiene el apoyo del licenciador, o lo recibe por el uso que hace.

.. image:: ./images/sa.large.png
   :height: 50px
   :width: 50px
   :align: left

**Compartir Igual** - Si remezcla, transforma o crea a partir del material, deberá difundir sus contribuciones bajo la misma licencia que el original [3]_.

----

*No hay restricciones adicionales* - No puede aplicar términos legales o medidas tecnológicas que legalmente restrinjan realizar aquello que la licencia permite.

.. [1] Si se facilitan, debe proporcionar el nombre del creador y las partes a reconocer, un aviso de derechos de explotación, un aviso de licencia, una nota de descargo de responsabilidad y un enlace al material. Las licencias CC anteriores a la versión 4.0 también requieren que facilite el título del material, si le ha sido facilitado, y pueden tener algunas otras pequeñas diferencias.

.. [2] En la versión 3.0 y anteriores, la indicación de los cambios sólo se requiere si se crea una obra derivada.

.. [3] También puede utilizar una licencia compatible de la lista de https://creativecommons.org/compatiblelicenses.
